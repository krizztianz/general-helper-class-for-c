﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using MVC4FW.Models;
using BCrypt.Net;

namespace MVC4FW.Lib
{
    public class Auth
    {
        public static void AdminLogin(User user)
        {
            HttpContext.Current.Session.Add("admin-userid", user.userid);
            HttpContext.Current.Session.Add("admin-username", user.username);
            HttpContext.Current.Session.Add("admin-name", user.name);
            HttpContext.Current.Session.Add("admin-email", user.email);
            HttpContext.Current.Session.Add("admin-password", user.password);
            HttpContext.Current.Session.Add("admin-level", user.level);
        }

        public static User GetAdminUser()
        {
            try
            {
                User user = new User();
                user.userid = Convert.ToInt32(HttpContext.Current.Session["admin-userid"].ToString());
                user.username = HttpContext.Current.Session["admin-username"].ToString();
                user.name = HttpContext.Current.Session["admin-name"].ToString();
                user.email = HttpContext.Current.Session["admin-email"].ToString();
                user.password = HttpContext.Current.Session["admin-password"].ToString();
                user.level = Convert.ToInt32(HttpContext.Current.Session["admin-level"].ToString());

                return user;
            }
            catch (Exception e)
            {
                return null;
            }
        }

        public static void MemberLogin(User user)
        {
            HttpContext.Current.Session.Add("member-userid", user.userid);
            HttpContext.Current.Session.Add("member-username", user.username);
            HttpContext.Current.Session.Add("member-name", user.name);
            HttpContext.Current.Session.Add("member-email", user.email);
            HttpContext.Current.Session.Add("member-password", user.password);
            HttpContext.Current.Session.Add("member-level", user.level);
        }

        public static User GetMemberUser()
        {
            try
            {
                User user = new User();
                user.userid = Convert.ToInt32(HttpContext.Current.Session["member-userid"].ToString());
                user.username = HttpContext.Current.Session["member-username"].ToString();
                user.name = HttpContext.Current.Session["member-name"].ToString();
                user.email = HttpContext.Current.Session["member-email"].ToString();
                user.password = HttpContext.Current.Session["member-password"].ToString();
                user.level = Convert.ToInt32(HttpContext.Current.Session["member-level"].ToString());

                return user;
            }
            catch (Exception e)
            {
                return null;
            }
        }

        public static void Logout()
        {
            HttpContext.Current.Session.Abandon();
            HttpContext.Current.Session.RemoveAll();
        }

        //Encrypt String to BCrypt
        public static string EncryptStringBCrypt(string password)
        {
            try
            {
                string salt = BCrypt.Net.BCrypt.GenerateSalt();
                string passwd = BCrypt.Net.BCrypt.HashPassword(password, salt);
                return passwd;
            }
            catch (Exception e)
            {
                return null;
            }
        }

        //Verify Password against BCrypt hash
        public static Boolean VerifyPasswordBCrypt(string password, string hash)
        {
            try
            {
                return BCrypt.Net.BCrypt.Verify(password, hash);
            }
            catch (Exception e)
            {
                return false;
            }
        }
    }
}